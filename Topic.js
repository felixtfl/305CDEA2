var request = require("request");

var weatherapiuil = "http://api.openweathermap.org/data/2.5/weather?q=HongKong&appid=79e463fc11079a8da6f15c4a06da85ef&units=metric";

exports.writeToFirebase = function(firebase, id, username, title, content, callback) {
    var date = new Date().toJSON();
    var weather = "";
    var temp = "";
    // get the weather infomation
    request(weatherapiuil, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var data2 = JSON.parse(body);
            temp = data2["main"]["temp"];
            weather = data2["weather"][0]["main"];
            //init the data
            var data = {
                id: id,
                username: username,
                title: title,
                content: content,
                created: date,
                weather: weather,
                temp: temp
            };

            firebase.database().ref('Topic/' + id).set(data);
            callback({
                code: 200,
                response: {
                    status: 'success',
                    message: 'Topic inserted',
                    data: data
                }
            });
        }
    });
};

//delete the topic form the id
exports.deleteFid = function(firebase, id, callback) {
    if (typeof id === "undefined") {
        callback({
            code: 400,
            response: {
                status: 'error',
                message: 'missing id'
            }
        });
    }
    callback({
        code: 200,
        response: {
            status: 'success',
            message: 'Topic Deleted'
        }
    });
    try {
        firebase.database().ref('Topic/' + id).remove();
    }
    catch (err) {
        console.log(err.message);
        callback({
            code: 404,
            response: {
                status: 'error',
                message: 'Topic not found'
            }
        });
    }
};
