var frisby = require('frisby')

frisby.create('list a topic')
    .get('https://assignment305cde-felixtfl.c9users.io/get/1493217866217')
    .expectStatus(200)
    .expectJSONTypes({
        "username": String,
        "title": String,
        "content": String,
        "created": String,
        "weather": String,
        "temp": Number
    }).toss()

frisby.create('list all topic')
    .get('https://assignment305cde-felixtfl.c9users.io/all')
    .expectStatus(200)
    .toss()

frisby.create('add a new topic')
    .post('https://assignment305cde-felixtfl.c9users.io/add', {
        "username": "user123",
        "title": "emoji is ok !!!😂",
        "content": "haha 😂😂😂😂"
    }, {
        json: true
    })
    .expectStatus(200)
    .expectJSONTypes({
        "status": String,
        "message": String,
        "data": {
            "id": Number,
            "username": String,
            "title": String,
            "content": String,
            "created": String,
            "weather": String,
            "temp": Number
        }
    }).toss()

frisby.create('update a topic')
    .post('https://assignment305cde-felixtfl.c9users.io/update/1493237886745', {
        "username": "userupdate",
        "title": "updated title",
        "content": "this is updated content"
    }, {
        json: true
    })
    .expectStatus(200)
    .expectJSONTypes({
        "status": String,
        "message": String,
        "data": {
            "username": String,
            "title": String,
            "content": String,
            "created": String,
            "weather": String,
            "temp": Number
        }
    }).toss()

frisby.create('delete not exists topic')
    .delete('https://assignment305cde-felixtfl.c9users.io/delete')
    .expectStatus(404)
    .expectHeaderContains('Content-Type', 'application/json')
    .expectJSON({code: 'ResourceNotFound', message: '/delete does not exist'})
    .toss()

frisby.create('delete a topic')
    .delete('https://assignment305cde-felixtfl.c9users.io/delete/1493237886745')
    .expectStatus(200)
    .expectHeaderContains('Content-Type', 'application/json')
    .expectJSON({status: 'success', message: 'Topic Deleted'})
    .toss()
