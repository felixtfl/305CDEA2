var restify = require('restify');
var server = restify.createServer();
var firebase = require("firebase");

// import topic module
var Topic = require("./Topic.js");

server.use(restify.queryParser());
server.use(restify.authorizationParser());
server.use(restify.bodyParser());

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDYHRuVXjpwHwO-wgXRoWBSQbp3YQ5QLv0",
    authDomain: "cde-4262c.firebaseapp.com",
    databaseURL: "https://cde-4262c.firebaseio.com",
    projectId: "cde-4262c",
    storageBucket: "cde-4262c.appspot.com",
    messagingSenderId: "982232846718"
};
firebase.initializeApp(config);

server.post(/^\/update\/([0-9]*)$/, topicHandler);
server.post('/add', topicHandler);

server.del(/^\/delete\/([0-9]*)$/, deletehandler);

server.get(/^\/get\/([0-9]*)$/, getHandler);

server.get('/all', getHandler);

server.get('/', function(req, res, next) {
    //redirect to /all
    res.setHeader('Location', '/all');
});

//delete topic
function deletehandler(req, res, next) {
    // delete the topic form the id
    Topic.deleteFid(firebase, req.params[0],
        function(data) {
            res.send(data.code, data.response);
            res.end();
        });
}

//get one topic or all
function getHandler(req, res, next) {
    var data;
    // if no params then get all
    if (typeof req.params[0] === "undefined") {
        data = firebase.database().ref('Topic/');
    }
    else {
        //get the specific topic
        data = firebase.database().ref('Topic/' + req.params[0]);
    }
    // get the data
    data.once("value", function(snapshot) {
        res.send(snapshot.val());
    }, function(errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
}

//create/update topic
function topicHandler(req, res, next) {
    // make sure the request body is json format
    if (req.headers['content-type'] != 'application/json') {
        return next(new restify.BadRequestError("body not json formatted"));
    }
    var data = req.body;
    var id = 0; // init id
    //if there are no params
    if (typeof req.params[0] === "undefined") {
        // generate id
        id = new Date().getTime();
    }
    else {
        //get id from the url
        id = req.params[0];
    }

    // call the method write to firebase
    Topic.writeToFirebase(firebase, id, data["username"], data["title"], data["content"], function(data) {
        res.setHeader('content-type', 'application/json');
        res.send(data.code, data.response);
        res.end();
    });
}

server.listen(8080, function() {
    console.log('incoming request being handled');
});
